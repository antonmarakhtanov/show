import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, mergeMap, shareReplay } from 'rxjs/operators';

// Хмм, обычный самопальный стейт сервис

@Injectable()
export class EventsStateService {
  private events$?: Observable<EventModel<{}>[]>;

  constructor(
    private eventsApiService: EventApiService,
    private eventTypeService: EventTypeService,
    private spinnerService: SpinnerService
  ) {}

  getEvents(
    deviceType: string,
    eventTypeName: string,
    disableCache?: boolean
  ): Observable<EventModel<{}>[]> {
    if (disableCache || !this.events$) {
      this.events$ = this.initialLoadEvents();
    }

    return this.eventTypeService
      .getEventTypeId(deviceType, eventTypeName)
      .pipe(mergeMap(eventTypeId => this.getEventsByEventTypeId(eventTypeId)));
  }

  private initialLoadEvents() {
    return this.eventTypeService.getAll().pipe(
      mergeMap(eventTypes => {
        const eventTypeIds = eventTypes.map(eventType => eventType.id);
        
        return this.spinnerService.addSpinner(
          this.eventsApiService.getByTypes(eventTypeIds)
        );
      }),
      shareReplay(1)
    );
  }

  private getEventsByEventTypeId(eventTypeId: string) {
    return this.events$!.pipe(
      map(events => events.filter(event => eventTypeId === event.eventTypeId))
    );
  }
}
