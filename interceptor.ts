import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
  } from '@angular/common/http';
  import { Injectable } from '@angular/core';
  import { Observable } from 'rxjs';

  // Тоже обычный интерсептор для работы с токенами, вроде даже не дописаный, но рабочий,
  // вроде как в далеких планах его планировалось доделать
  
  @Injectable()
  export class ApiInterceptor implements HttpInterceptor {
    constructor(private tokenStoreService: TokenStoreService) {}
  
    intercept(
      req: HttpRequest<any>,
      next: HttpHandler
    ): Observable<HttpEvent<any>> {
      const headers = req.headers
        .set(ApiConstants.headers.contentType, ApiConstants.contentType.json)
        .set(ApiConstants.headers.authorization, this.getToken());
  
      const authReq = req.clone({ headers });
  
      return next.handle(authReq);
    }
  
    private getToken() {
      const token = this.tokenStoreService.getEncodedToken();
  
      return `${ApiConstants.tokenType} ${token}`;
    }
  }
  