import { Injectable, Injector } from '@angular/core';

import { FooBarcodeListConfigurationService } from './Foo-barcode-list.service';
import { supportedEventTypes } from '@core/constants';

// На самом деле это интересный маленький сервис позволяет сделать 
// так называемую абстрактную фабрику сервисов. Что я имею ввиду -
// есть два поля в этом сервисе - eventType и deviceType которые так сказать определяют
// 2-ух мерную сетку возможных приложений 
// (например eventType1, deviceType2, eventType4, deviceType1),
// (отдельных поднимающихся микросервисов через Angular Module Federation https://www.angulararchitects.io/en/aktuelles/the-microfrontend-revolution-part-2-module-federation-with-angular/)
// и связать компоненту которую нужно отрисовать - это getComponentType(),
// и данный сервис предоставляет инжектор (как вы наверно поняли для работы этой компоненты)
// который потом подхватывается в соседнем сервисе и инжектится в нужном приложении

// Сделано это в целях формирования микросервисной архитектуры - все приложения в своих репах, работают раздельно,
// таким образом достаточно предоставить вот этот файл в корневое приложение и предоставить по токену EVENT_LIST_CONFIGURATION_PROVIDER_TOKEN
// конфиргурейшн сервисы для работ компонент (тут можно глазком глянуть в Barcode папку)

@Injectable()
export class ListComponentProvider
  implements IListComponentProvider
{
  eventType = supportedEventTypes.barcode.eventType;
  deviceType = supportedEventTypes.barcode.device;

  getComponentType() {
    return EventListComponent;
  }

  registerComponentServices(parent: Injector) {
    return Injector.create({
      providers: [
        {
          provide: EVENT_LIST_CONFIGURATION_PROVIDER_TOKEN,
          useClass: FooBarcodeListConfigurationService,
          deps: [],
        },
      ],
      parent,
    });
  }
}
