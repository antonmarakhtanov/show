import { Component, Inject, Injector, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// В чем суть - эта компонента отрисовывает лист компонент для самых разных 
// сочетаний eventType и deviceType которые вернет eventTypesService.getAll() и которые 
// будут запровадены в данное приложение по токену LIST_COMPONENT_PROVIDER_TOKEN
// тоесть всевозможные List-компоненты (папка eventList)

// да, понимаю, возникла сложность что на этой странице в итоге будет лист List-компонент,
// и у меня на работе иногда вызывали небольшие затруднения данная небольшая неразбериха

// LIST_COMPONENT_PROVIDER_TOKEN = ListComponentProvider из файла injectableProviderService.ts,
// для удобства которого я вынес в файл рядом

@Component({
  selector: 'events-page',
  templateUrl: './events-page.component.html',
  styleUrls: ['./events-page.component.scss'],
  providers: [EventsStateService],
})
export class EventsPageComponent implements OnInit, OnDestroy {
  eventTypeListComponents: { component: any; injector: Injector }[] = [];
  isLoading = true;
  private destroy$ = new Subject<void>();

  constructor(
    private eventTypesService: EventTypeApiService,
    @Inject(LIST_COMPONENT_PROVIDER_TOKEN)
    private listComponentProviders: IListComponentProvider[],
    private snackBar: MatSnackBar,
    private injector: Injector
  ) {}

  ngOnInit() {
    this.loadTypes();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  private loadTypes() {
    this.eventTypesService
      .getAll()
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        eventTypes => {
          this.isLoading = false;
          this.handleTypesLoaded(eventTypes);
        },
        () => {
          this.isLoading = false;
        }
      );
  }


  // вот тут происходит отбор нужных компонент которые нужно отрисовать
  private handleTypesLoaded(data: EventType[]) {
    data.forEach(eventType => {
      const componentProvider = this.listComponentProviders.find(
        provider =>
          provider.eventType == eventType.name &&
          provider.deviceType == eventType.deviceType
      );

      if (componentProvider) {
        const component = componentProvider.getComponentType();
        // тут происходит расширение базового инжектора приложения новым инжектором который дополнится
        // нужным для компоненты конфинурейшн сервисом
        const injector = componentProvider.registerComponentServices(
          this.injector
        );

        this.eventTypeListComponents.push({ component, injector });
      } else {
        this.snackBar.open(
          `no component provider for type ${JSON.stringify(eventType)}`
        );
      }
    });
  }
}
