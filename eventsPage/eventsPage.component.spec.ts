import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Observable, of, throwError } from 'rxjs';

import apiServiceTestData from '@test-data/api/services/event-type-api.service.json';

class AppConfigServiceMock {
  getConfig() {
    return APP_CONFIG;
  }
}

export class MockComponentProvider implements IListComponentProvider {
  eventType = 'SampleEventType1';
  deviceType = 'SampleDeviceType1';

  getComponentType() {
    return null;
  }

  registerComponentServices() {
    return null as any;
  }
}

describe('EventsPageComponent', () => {
  let component: EventsPageComponent;
  let fixture: ComponentFixture<EventsPageComponent>;
  let service: EventTypeApiService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      declarations: [EventsPageComponent],
      providers: [
        {
          provide: LIST_COMPONENT_PROVIDER_TOKEN,
          useClass: MockComponentProvider,
          multi: true,
        },
        { provide: AppConfigService, useClass: AppConfigServiceMock },
        EventTypeApiService,
        DataService,
        {
          provide: SpinnerService,
          useValue: {
            addSpinner: <T>(o: Observable<T>) => o,
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EventsPageComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(EventTypeApiService);
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should not load all events (failed route scenario)', () => {
      const spy = spyOn(service, 'getAll').and.returnValue(throwError('500'));

      fixture.detectChanges();
      expect(spy.calls.count()).toEqual(1);
      expect(component.isLoading).toBeFalsy();
    });

    it('should load all events', () => {
      const { getAllEvents } = apiServiceTestData;
      const spy = spyOn(service, 'getAll').and.returnValue(of(getAllEvents));

      fixture.detectChanges();
      expect(spy.calls.count()).toEqual(1);
      expect(component.isLoading).toBeFalsy();
      expect(component.eventTypeListComponents).toEqual([]);
    });
  });
});
