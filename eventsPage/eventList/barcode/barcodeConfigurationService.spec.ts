import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('FooBarcodeListConfigurationService', () => {
  let service: FooBarcodeListConfigurationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'Foo-barcode/create',
            component: CreateEventPageComponent,
          },
        ]),
        FooBarcodeModule,
      ],
      providers: [FooBarcodeListConfigurationService],
    });

    service = TestBed.inject(FooBarcodeListConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getUpdateEventRouterLink', () => {
    it('should return right link to update page', () => {
        // TestData тут берется из файла json рядом потому и такая страшная конвертация,
        // просто tslint ругается, но ничего страшного не было, а мокать все поля в данном интерфейсе
        // для проверки на одно поле считается неуместным и только вредит (в json файле только одно поле)
      const eventModel = testData.eventModel as unknown as EventModel<{}>;
      const result = service.getUpdateEventRouterLink(eventModel);

      expect(result).toEqual('Foo-barcode/update/1');
    });
  });

  describe('convertToListItem', () => {
    it('should return event with verificationType', () => {
        // а вот здесь почемуто я не вынес в отдельный файл этот мок, уже и не помню почему но для 
        // чистоты оставлю как есть
      const event = new EventModel<{}>({
        payloadModel: { advancedVerification: true },
      });

      const result = service.convertToListItem(event);

      expect(result).toBeDefined();
      expect((result as any).verificationType).toEqual(testData.expectedVerificationType);
    });
  });

  describe('convertToListItem', () =>
    it('should return event with updateDate without seconds', () => {
      const eventModel = new EventModel<{}>({
        updateDate: new Date(testData.eventModel.updateDate),
      });

      const actualResult = (service.convertToListItem(eventModel) as any)
        .updateDate;

      expect(actualResult).toEqual(testData.expectedUpdateDate);
    }));
});
