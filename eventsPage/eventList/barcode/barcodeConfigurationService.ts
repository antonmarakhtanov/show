import { Injectable } from '@angular/core';

// Это сервис который содержит в себе информацию по некому Barcode листу компоненте и 
// задает начальную конфигурацию. Этот класс реализует IEventListConfigurationService,
// и для лучшего понимания советую сначала разобратся в папке выше

@Injectable()
export class FooBarcodeListConfigurationService
  implements IEventListConfigurationService
{
  constructor() {}

  eventType = supportedEventTypes.barcode.eventType;
  deviceType = supportedEventTypes.barcode.device;

  title = 'Foo.Events.Barcode.Label';
  createEventRouterLink = 'Foo-barcode/create';
  columns: ListColumn[] = [
    {
      field: 'name',
      title: 'Foo.Events.Barcode.Name.Label',
      type: FooType.text,
    },
    {
      field: 'updateDate',
      title: 'Foo.Events.Barcode.LastModified.Label',
      type: FooType.Datetime,
    },
    {
      field: 'category',
      title: 'Foo.Events.Barcode.Category.Label',
      enumLocalizationPrefix: enumLocalizationPrefix.category,
      type: FooType.Enum,
    },
    {
      field: 'payloadModel.dataType',
      title: 'Foo.Events.Barcode.DataType.Label',
      enumLocalizationPrefix: enumLocalizationPrefix.dataType,
      type: FooType.Enum,
    },
    {
      field: 'payloadModel.isPrivate',
      title: 'Foo.Events.Barcode.IsPrivate.Label',
      type: FooType.Boolean,
    },
  ];

  getUpdateEventRouterLink(barcode: EventModel<{}>) {
    return `Foo-barcode/update/${barcode.id}`;
  }

  // это сделано для конвертации даты и показа на экране
  convertToListItem(event: EventModel<{}>): ListItem {
    return {
      ...event,
      updateDate: event.updateDate && trimSeconds(event.updateDate),
      // а вот это зачем было сделано я честно не помню, возможно для согласования с моделю ListItem
      event,
    };
  }
}
