import {
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Итак - это компонента которая отрисовывает некую таблицу с евентами
// (в данном случае в примере будет выступать Barcode-event, что находится на папку глубже)

@Component({
  selector: 'event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
})
export class EventListComponent implements OnInit {
  @ViewChild(ListComponent) list!: ListComponent;

  readonly createEventRouterLink: string;
  readonly title: string;
  readonly columns: ListColumn[];

  data: EventModel<{}>[] = [];
  listData: ListItem[] = [];
  expanded: boolean = true;

  searchFilter: SearchData;
  searchAvailableValues: string[] = [];

  constructor(
    @Inject(EVENT_LIST_CONFIGURATION_PROVIDER_TOKEN)
    private configurationService: IEventListConfigurationService,
    private view: ViewContainerRef,
    private eventApiService: EventApiService,
    private confirmation: BctConfirmationDialogService,
    private router: Router,
    private eventsStateService: EventsStateService,
    private activatedRoute: ActivatedRoute,
    @Inject(PERMISSIONS_SERVICE_TOKEN)
    private permissionsService: IPermissionsService
  ) {
    this.title = configurationService.title;
    this.createEventRouterLink = configurationService.createEventRouterLink;
    this.columns = configurationService.columns;
    this.searchFilter = { value: '', column: this.columns[0] };
  }

  get isReadonly() {
    return !this.permissionsService.hasConfigurePermission();
  }

  ngOnInit(): void {
    this.loadItems();
  }

  async update(event: EventModel<{}>): Promise<void> {
    const updateUrl = this.configurationService.getUpdateEventRouterLink(event);
    await this.router.navigate([updateUrl], {
      relativeTo: this.activatedRoute,
    });
  }

  delete(item: EventModel<{}>): void {
    this.confirmation.show(this.view, {
      onOk: async () => {
        this.eventApiService.deleteEvent(item).subscribe(() => {
          this.loadItems(true);
        });
      },
      onCancel: () => {},
      resourceKey: 'Common.Events.Delete.ConfirmationDialog.Text',
      resourceStringParameters: [item.name],
    });
  }

  expandPanel() {
    this.expanded = true;
  }

  onFilterColumnChange(column: SearchColumn) {
    this.searchAvailableValues = this.list.getTextColumnStringValues(
      column.field
    );
  }

  private loadItems(disableCache?: boolean) {
    const { deviceType, eventType } = this.configurationService;
    this.eventsStateService
      .getEvents(deviceType, eventType, disableCache)
      .subscribe(data => {
        this.data = data;
        this.listData = this.data.map(event =>
          this.configurationService.convertToListItem
            ? this.configurationService.convertToListItem(event)
            : { ...event, event }
        );

        setTimeout(() => this.onFilterColumnChange(this.searchFilter.column));
      });
  }
}
